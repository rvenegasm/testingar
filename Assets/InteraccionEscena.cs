﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteraccionEscena : MonoBehaviour
{
    public bool activado = false;
    public GameObject player;
    public string[] accion;
    public float turnSpeed = 10;
    public Transform[] estaciones;
    public Transform posInicial;
    public AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {

#if UNITY_ANDROID

        StartCoroutine(go());
#endif

    }

    // Update is called once per frame


    void Update()
    {
#if UNITY_STANDALONE

       if (!activado)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Activando la interacción");
               
               StartCoroutine(go());
            }
            activado = false;
        }
#endif

    }

    IEnumerator go()
    {

        yield return new WaitForSeconds(3);
        audio.Play();
        
        for (int i=0; i < estaciones.Length; i++)
        {
            Debug.Log("moviendo a "+estaciones[i].name+" con la acción "+accion[i]);
            player.transform.position = estaciones[i].position;
            player.GetComponent<Animator>().SetTrigger(accion[i]);
            yield return new WaitForSeconds(3);
        }

        Debug.Log("moviendo a posicion inicial");
        player.transform.position = posInicial.position;
        player.GetComponent<Animator>().SetFloat("Blend", turnSpeed);

    }
}
